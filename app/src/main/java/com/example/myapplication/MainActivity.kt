package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val controler = findNavController(R.id.Nav_Fragment)
        val appBarConfig = AppBarConfiguration(
            setOf(
                R.id.loginActivity,
                R.id.registerActivity


            )
        )
        setupActionBarWithNavController(controler,appBarConfig)
    }
}