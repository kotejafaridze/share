package com.example.myapplication.Fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.myapplication.R

class LoginActivity: Fragment(R.layout.login_layout) {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonLogin: Button
    private lateinit var buttonRegistration: Button
    private lateinit var buttonResetPassword: Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonRegistration= view.findViewById(R.id.buttonRegistration)
        editTextEmail = view.findViewById(R.id.editTextEmail)
        editTextPassword = view.findViewById(R.id.editTextPassword)
        buttonLogin = view.findViewById(R.id.buttonLogin)
        buttonResetPassword = view.findViewById(R.id.buttonResetPassword)

        val controler = Navigation.findNavController(view)
        buttonRegistration.setOnClickListener {
            val action = LoginActivityDirections.actionLoginActivityToRegisterActivity()
            controler.navigate(action)


        }
        buttonResetPassword.setOnClickListener {
            val action1 = LoginActivityDirections.actionLoginActivityToResetPasswordActivity()
            controler.navigate(action1)
        }

    }

}